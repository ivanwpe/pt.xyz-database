create database PT_XYZ

use PT_XYZ

create table Users(
    userId int identity(1,1) PRIMARY KEY,
    videoId int FOREIGN KEY REFERENCES Video(videoId),
    newsId int FOREIGN KEY REFERENCES News(newsId),
    forumId int FOREIGN KEY REFERENCES Forum(forumId),
    promoId int FOREIGN KEY REFERENCES Promo(promoId),
    userName VARCHAR(255) NOT NULL,
    userEmail VARCHAR(255) NOT NULL,
    userPassword VARCHAR(255) NOT NULL,
    userPremium BIT NOT NULL,
    userLoggedIn datetimeoffset DEFAULT GETDATE(),
    userStatus VARCHAR(255) NOT NULL
)

create table Admins(
    adminId int identity(1,1) PRIMARY KEY,
    newsId int FOREIGN KEY REFERENCES News(newsId),
    promoId int FOREIGN KEY REFERENCES Promo(promoId),
    forumId int FOREIGN KEY REFERENCES Forum(forumId),
    adminName VARCHAR(255) NOT NULL
)

create table Video(
    videoId int identity(1,1) PRIMARY KEY,
    videoTitle VARCHAR(255) NOT NULL,
    videoPremium BIT NOT NULL
)

create table News(
    newsId int identity(1,1) PRIMARY KEY,
    newsTitle VARCHAR(255) NOT NULL,
    newsDate datetimeoffset DEFAULT GETDATE()
)

create table Forum(
    forumId int identity(1,1) PRIMARY KEY,
    forumTitle VARCHAR(255) NOT NULL,
    forumAddedDate datetimeoffset DEFAULT GETDATE(),
    forumUpdatedDate datetimeoffset DEFAULT GETDATE(),
    reportId int identity(1,1)
)

create table promo(
    promoId int identity(1,1) PRIMARY KEY,
    promoName VARCHAR(255) NOT NULL,
    promoEnd datetimeoffset NOT NULL
)

create table Ad(
    adId int identity(1,1) PRIMARY KEY,
    videoId int FOREIGN KEY REFERENCES Video(videoId),
    adTitle VARCHAR(255) NOT NULL
)
